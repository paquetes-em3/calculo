<?php
namespace desarrollo_em3\calculo;
use DateTime;
use desarrollo_em3\error\error;
use Exception;

class calculo{
    private function ajusta_fecha(string $fecha){
        if($fecha === ''){
            return (new error())->error( 'Error la fecha no puede venir vacia', $fecha);
        }
        $fecha_parser = explode('/',$fecha);
        if(count($fecha_parser)>1){
            $fecha = $fecha_parser[2].'-'.$fecha_parser[1].'-'.$fecha_parser[0];
        }
        return $fecha;
    }

    /**
     * TRASLADADO
     * Calcula la distancia entre dos puntos geográficos utilizando sus coordenadas.
     *
     * Esta función calcula la distancia en metros entre dos puntos dados sus latitudes y longitudes
     * utilizando la fórmula del haversine. El resultado es la distancia en línea recta entre los dos
     * puntos en la superficie de la Tierra.
     *
     * @param float $lat1 Latitud del primer punto.
     * @param float $lgn1 Longitud del primer punto.
     * @param float $lat2 Latitud del segundo punto.
     * @param float $lgn2 Longitud del segundo punto.
     *
     * @return float Devuelve la distancia entre los dos puntos en metros.
     */
    final public function calcula_distancia_con_coordenadas(float $lat1, float $lgn1, float $lat2, float $lgn2){
        $R = 6371; // radio de la tierra por KM

        $hav = pow(sin($this->toRad($lat1-$lat2)/2) ,2) +
            cos($this->toRad($lat1)) * cos($this->toRad($lat2)) *
            pow(sin($this->toRad($lgn1 - $lgn2)/2) ,2);

        $distancia = $R * (2 * atan2(sqrt($hav),sqrt(1-$hav)));

        $distancia *= 1000;

        return $distancia;
    }

    final public function diferencia_dias(string $fecha_ini, string $fecha_fin)
    {
        $fecha_inicial = $this->ajusta_fecha($fecha_ini);
        if(error::$en_error){
            return (new error())->error(  'Error la ajuste fecha inicial', $fecha_inicial);
        }
        $fecha_final = $this->ajusta_fecha($fecha_fin);
        if(error::$en_error){
            return (new error())->error(   'Error la ajuste fecha final', $fecha_final);
        }
        try {
            $date1 = new DateTime($fecha_inicial);
            $date2 = new DateTime($fecha_final);
            $diff = $date1->diff($date2);
            return (int)trim($diff->days);
        }
        catch (Exception $e){
            return (new error())->error(   'Error al obtener diferencia', $e);
        }
    }

    private function toRad(float $value): float
    {
        return $value * pi() / 180;
    }

}
<?php
namespace desarrollo_em3\test;
use desarrollo_em3\calculo\calculo;
use desarrollo_em3\error\error;
use desarrollo_em3\liberator\liberator;
use desarrollo_em3\reportes\sql;
use PDO;
use PHPUnit\Framework\TestCase;
use stdClass;

class calculoTest extends TestCase
{
    private PDO $link;
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $_GET['test_new'] = 1;
        require_once '/var/www/html/em3/requires.php';
        require_once '/var/www/html/em3/config/seguridad.php';
        $_SESSION['numero_empresa'] = 1;
        /*$conexion = new \conexion();
        $this->link = $conexion->link;*/

    }

    final public function test_calcula_aguinaldo_dias()
    {
        error::$en_error = false;
        $obj = new calculo();
        //$obj = new liberator($obj);

        $lat1 = -1;
        $lgn1 = -1;
        $lat2 = -1;
        $lgn2 = -1;
        $result = $obj->calcula_distancia_con_coordenadas($lat1,$lgn1,$lat2,$lgn2);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);
        error::$en_error = false;
        $lat1 = -10;
        $lgn1 = -11;
        $lat2 = -10;
        $lgn2 = -11.0001;
        $result = $obj->calcula_distancia_con_coordenadas($lat1,$lgn1,$lat2,$lgn2);
        $this->assertNotTrue(error::$en_error);
        $this->assertIsFloat($result);

        error::$en_error = false;


    }



}

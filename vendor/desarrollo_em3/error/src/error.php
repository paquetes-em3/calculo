<?php
namespace desarrollo_em3\error;
class error{

    public string $mensaje = 'Error desconocido';
    public string $mensaje_limpio = 'Error desconocido';
    public string $class;
    public $line;
    public $file;
    public $data;
    public $error;
    public $function;
    public $data_error;
    public $header;
    public static bool $en_error = false;


    public function error(string $mensaje , $data,string $class = '',string $file = '',string $function = '',
                          string $line = '', int $n_error_mysql = 0, string $error_mysql = '',
                          array $referencias = array(), bool $api_format = false): array{

        $debug = debug_backtrace(2);

        $class = trim($class);

        if($class === ''){
            if(isset($debug[1]['class'])){
                $class = $debug[1]['class'];
            }
        }

        $file = trim($file);
        if($file === ''){
            if(isset($debug[0]['file'])){
                $file = $debug[0]['file'];
            }
        }

        $line = trim($line);
        if($line === ''){
            if(isset($debug[0]['line'])){
                $line = $debug[0]['line'];
            }
        }

        $function = trim($function);
        if($function === ''){
            if(isset($debug[1]['function'])){
                $function = $debug[1]['function'];
            }
        }

        $this->error = 1;
        $this->mensaje_limpio = trim($mensaje);
        $this->mensaje = "<font size='2'><div><b style='color: brown'>$mensaje</b></div>";
        $this->class = "<div><b>$class</b></div>";
        $this->function = "<div><b>$function</b></div>";
        $this->file = "<div><b>$file</b></div>";
        $this->line = "<div><b>$line</b></div><br></font>";
        $this->data = $data;

        $this->data_error = array('<font size="2">','mensaje'=>'Exito','class'=>$this->class,'function'=>$this->function,
            'file'=>$this->file,'line'=>$this->line,'</font><hr><font size="1">',
            'data'=>$this->data,'</font>','n_error_mysql'=>$n_error_mysql,'error_mysql'=>$error_mysql,
            'mensaje_limpio'=>$this->mensaje_limpio);
        if($this->error === 1){
            if($api_format === true){
                $this->data_error = array();
            }
            $this->data_error['error'] = 1;
            $this->data_error['mensaje'] = $this->mensaje;
            $this->data_error['data'] = $this->data;
            $this->data_error['n_error_mysql'] = $n_error_mysql;
            $this->data_error['error_mysql'] = $error_mysql;
            $this->data_error['referencias'] = $referencias;
            $this->data_error['mensaje_limpio'] = $this->mensaje_limpio;
        }
        self::$en_error = true;
        return $this->data_error;
    }

    public function datos($error,$mensaje , $class, $line,$file,$data, $function,  $n_error_mysql = 0,
                          $error_mysql = '',$referencias = array(), $api_format = false): array{
        $this->error = $error;
        $this->mensaje = "<font size='2'><div><b style='color: brown'>$mensaje</b></div>";
        $this->class = "<div><b>$class</b></div>";
        $this->function = "<div><b>$function</b></div>";
        $this->file = "<div><b>$file</b></div>";
        $this->line = "<div><b>$line</b></div><br></font>";
        $this->data = $data;

        $this->data_error = array('<font size="2">','mensaje'=>'Exito','class'=>$this->class,'function'=>$this->function,
            'file'=>$this->file,'line'=>$this->line,'</font><hr><font size="1">',
            'data'=>$this->data,'</font>','n_error_mysql'=>$n_error_mysql,'error_mysql'=>$error_mysql);
        if($error === 1){
            if($api_format === true){
                $this->data_error = array();
            }
            //self::$en_error = true;
            $this->data_error['error'] = 1;
            $this->data_error['mensaje'] = $this->mensaje;
            $this->data_error['data'] = $this->data;
            $this->data_error['n_error_mysql'] = $n_error_mysql;
            $this->data_error['error_mysql'] = $error_mysql;
            $this->data_error['referencias'] = $referencias;
        }
        self::$en_error = true;
        return $this->data_error;
    }
    public function es_error(string $mensaje, $data){
        $debug = debug_backtrace(2);
        $funcion_llamada = '';
        if(isset($debug[2]['function'])){
            $funcion_llamada = $debug[2]['function'];
        }
        $class_llamada = '';
        if(isset($debug[2]['class'])){
            $class_llamada = $debug[2]['class'];
        }
        $data_error['error'] = 1;
        $data_error['mensaje'] = '<b><span style="color:red">' . $mensaje . '</span></b>';
        $data_error['file'] = '<b>' . $debug[0]['file'] . '</b>';
        $data_error['line'] = '<b>' . $debug[0]['line'] . '</b>';
        $data_error['class'] = '<b>' . $debug[1]['class'] . '</b>';
        $data_error['function'] = '<b>' . $debug[1]['function'] . '</b>';
        $data_error['data'] = $data;

        $_SESSION['error_resultado'][] = $data_error;

        self::$en_error = true;
        $this->mensaje = $mensaje;
        $this->class = $debug[1]['class'];
        $this->line = $debug[0]['line'];
        $this->file = $debug[0]['file'];
        $this->function = $debug[1]['function'];
        if($data === null){
            $data = '';
        }
        $this->data = $data;
        return $data_error;
    }

}